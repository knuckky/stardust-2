using UnityEngine;
using System.Collections;

public class NoteRiddleText : MonoBehaviour {

	GUIText text; //reference to the guitexture component used for screen fading
	public float fadeSpeed = 0.5f;
	private Color col = Color.white;
	private int fade = 0;
	// Use this for initialization
	void Start () {
		text = GetComponent<GUIText> (); //Get the guitext component for reference
		text.pixelOffset = new Vector2 (Screen.width / 2, Screen.height / 2);
		col.a = 0;
	}
	
	// Update is called once per frame
	void Update () {
		//Fade in
		if (fade == 1) {
			if (col.a < 1){
				col.a += fadeSpeed * Time.deltaTime*2;
			} else {
				col.a = 1;
				fade = 0;
			}
			//Fade Out
		} else if (fade == -1) {
			if (col.a > 0){
				col.a -= fadeSpeed * Time.deltaTime;
			} else {
				col.a = 0;
				fade = 0;
			}
		}
		text.color = col;
	}

	public void FadeIn(){
		fade = 1;
	}

	public void FadeOut(){
		fade = -1;
	}
}

