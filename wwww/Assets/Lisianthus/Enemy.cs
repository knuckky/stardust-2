﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
    {
        public List<Vector3> patrolNodes = new List<Vector3>();
        Transform player;               // Reference to the player's position.
        NavMeshAgent nav;               // Reference to the nav mesh agent.
        int index;
        public bool chasing;
        public float detectRange = 6;
        public float giveUpRange = 10;

        void Awake ()
        {
            index = 0;
            // Set up the references.
            chasing = false;
            player = GameObject.FindGameObjectWithTag ("Player").transform;
            nav = GetComponent <NavMeshAgent> ();
        }


        void Update ()
        {
        
            //patrol
            if(!chasing)
            {
                if(Vector3.Distance(transform.position, patrolNodes[index]) <= 1)
                {
                    if(index < patrolNodes.Count-1)
                    {
                        ++index;
                    }
                    else
                    {
                        index = 0;
                    }
                    nav.SetDestination (patrolNodes[index]);
                }
            }
            //Debug.Log(Vector3.Distance(transform.position, player.position) < 6);
            if(Vector3.Distance(transform.position, player.position) <= detectRange)
            {
                chasing = true;
                nav.SetDestination(player.position);
            }   
            
            if(chasing && Vector3.Distance(transform.position, player.position) >= giveUpRange)
            {
                chasing = false;
                nav.SetDestination (patrolNodes[index]);
            }
        }
}