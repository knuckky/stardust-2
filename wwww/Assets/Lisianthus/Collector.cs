﻿using UnityEngine;
using System.Collections;

public class Collector : MonoBehaviour {

		public PlayerController player;
        public master gameMaster;
		private HUDBehaviour hud;
		public GameObject sparkle;
		public GameObject deathEffect;
		public GameObject splash;
		public GameObject itemGUI;
		private GameObject lanternLight;
		private bool death = false; //used for collision detection with hazzards
        
        void OnTriggerEnter(Collider thing)
		{
			if (thing.gameObject.tag == "Item")
			{
                Item newItem = thing.gameObject.GetComponent<Item>();
                
                GameObject gameMasterObject = GameObject.FindWithTag("GameController");
                gameMaster = gameMasterObject.GetComponent<master>();
				
				//GET HUD FOR ITEM EFFECTS
				GameObject hudObject = GameObject.FindWithTag("HUD");
				hud = hudObject.GetComponent<HUDBehaviour>();
                
                Debug.Log("You have Collected " + newItem.Name);
                
                gameMaster.itemsCollected[newItem.ID] = true;
				/*player.itemsCollected[player.numItems].Name = newItem.Name;
                player.itemsCollected[player.numItems].ID = newItem.ID;*/
				
				//Create particle effect
				Instantiate (sparkle, thing.transform.position, thing.transform.rotation);
                Destroy(thing.gameObject);
				
				//Create screen flash
				if (newItem.Name == "Puzzle Fragment!"){
					hud.FadeIn();
					hud.SetSprite (0);
				} else if (newItem.Name == "Lantern!"){
					hud.FadeIn();
					hud.SetSprite (1);
				} else if (newItem.Name == "Key Fragment!"){
					hud.GlowPulse ();
				}
		}
			
			//Hazzard detection and death
			if (thing.gameObject.tag == "Hazzard"){
				if (death == false){
					death = true;
					
					//SPLASH!
					Instantiate(splash, transform.position, transform.rotation);
					
					GameObject gameMasterObject = GameObject.FindWithTag("GameController");
					gameMaster = gameMasterObject.GetComponent<master>();

					if (gameMaster.isFading != -1){
						gameMaster.isFading = -1; //fade out and restart scene on death
					}
				}
			}

			//Enemy detection and death
			if (thing.gameObject.tag == "Enemy"){
				if (death == false){
					death = true;
				
					GameObject gameMasterObject = GameObject.FindWithTag("GameController");
					gameMaster = gameMasterObject.GetComponent<master>();
				
					Instantiate(deathEffect, Vector3.zero, transform.rotation);
				}
			}
		
			if (thing.gameObject.tag == "Door")
			{
				Door newDoor = thing.gameObject.GetComponent<Door>();
			
				GameObject gameMasterObject = GameObject.FindWithTag("GameController");
				gameMaster = gameMasterObject.GetComponent<master>();
			
			
				if (gameMaster.isFading != -1){
					AudioSource doorOpen = gameMasterObject.GetComponent<AudioSource>();//For playing audio
					doorOpen.Play ();
					gameMaster.nextPosition = newDoor.nextRoomPos;
					gameMaster.isFading = -1;
					gameMaster.SetNewScene(newDoor.nextScene); 
				}
				//previous scene change script moved to master.cs public function 'ChangeScene'
			}

			if (thing.gameObject.tag == "FoyerDoor")
			{
				Door newDoor = thing.gameObject.GetComponent<Door>();
			
				GameObject gameMasterObject = GameObject.FindWithTag("GameController");
				gameMaster = gameMasterObject.GetComponent<master>();
			
			if(gameMaster.itemsCollected[0] && gameMaster.itemsCollected[3] && gameMaster.itemsCollected[5] && gameMaster.itemsCollected[8] && gameMaster.itemsCollected[9])
				{
					if (gameMaster.isFading != -1){
						AudioSource doorOpen = gameMasterObject.GetComponent<AudioSource>();//For playing audio
						doorOpen.Play ();
						gameMaster.nextPosition = newDoor.nextRoomPos;
						gameMaster.isFading = -1;
						gameMaster.SetNewScene(newDoor.nextScene); 
					}
				}
			
			}
		
		}



	void Update()
	{
		if(Input.GetKeyDown("m"))
		{
			GameObject gameMasterObject = GameObject.FindWithTag("GameController");
			gameMaster = gameMasterObject.GetComponent<master>();
   	        gameMaster.itemsCollected[0] = true;
   	        gameMaster.itemsCollected[3] = true;
   	        gameMaster.itemsCollected[5] = true;
			gameMaster.itemsCollected[8] = true;
			gameMaster.itemsCollected[9] = true;
   	    }
   }
        
}