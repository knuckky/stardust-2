﻿using UnityEngine;
using System.Collections;

public class DeathEffectBehaviour : MonoBehaviour {

	GUITexture spook; //reference to the guitexture component used for screen fading
	public float fadeSpeed = 0.5f;
	public float timer = 2;
	// Use this for initialization
	void Start () {
		spook = GetComponent<GUITexture> (); //Get the guitexture component for reference
		spook.pixelInset = new Rect (0f, 0f, Screen.width, Screen.height);
	}
	
	// Update is called once per frame
	void Update () {
		//Fade to clear 
		spook.color = Color.Lerp (spook.color, Color.clear, fadeSpeed*Time.deltaTime);
		if (spook.color.a <= 0.05f)
		{
			spook.color = Color.clear;
		}
		if (timer > 0) {
			timer -= Time.deltaTime;
		} else {
			GameObject gameMasterObject = GameObject.FindWithTag("GameController");
			master gameMaster = gameMasterObject.GetComponent<master>();
			gameMaster.isFading = -1;
		}
	}
}
