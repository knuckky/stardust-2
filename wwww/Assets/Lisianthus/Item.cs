﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour 
{
    
    public string Name =  "";
    public int ID = -1;
    public master gameMaster;
    float timer;
    bool check;
    
    void awake()
    {
        timer = 0;
        check = false; 
    }
    
    
    void FixedUpdate()
    {
        if(timer < 0.1f)
        {
            timer += Time.deltaTime;
        }
        else if(!check)
        {
            GameObject gameMasterObject = GameObject.FindWithTag("GameController");
            gameMaster = gameMasterObject.GetComponent<master>();
            if(gameMaster.itemsCollected[ID] == true)
            {
                Destroy(gameObject);
            }
        }
    }
}
