﻿using UnityEngine;
using System.Collections;

public class SecretDoor : MonoBehaviour {

	float timer;
	bool timerActive;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//If the timer is active, then open then count down
		if (timerActive == true) {
			if (timer < 10){
				timer += Time.deltaTime;
			} else {
				//When the timer runs out, destroy the secret door and destroy self
				GameObject secretDoorObject = GameObject.FindWithTag("Secret");
				Destroy (secretDoorObject.gameObject);
				Destroy (gameObject);
			}
		}
	}

	//Start the timer to open the secret door when leon enters the trigger area
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player" && timerActive == false) {
			timerActive = true;
			Debug.Log ("Secret");
		}
	}

	//Stop and reset the secret door timer if leon leaves the trigger area
	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player" && timerActive == true) {
			timerActive = false;
			timer = 0;
			Debug.Log ("SecretLeave");
		}
	}
}
