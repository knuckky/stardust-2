﻿using UnityEngine;
using System.Collections;

public class LocationTitle : MonoBehaviour {
	
	public string title = "";
	public master gameMaster;
	private float time = 3;
	private bool done = false;
	GameObject gameMasterObject;
	
	// Use this for initialization
	void Start() {
		gameMasterObject = GameObject.FindWithTag("GameController");
		gameMaster = gameMasterObject.GetComponent<master>();
	}
	
	// Update is called once per frame
	void Update () {
		if (time > 0 && done == false) {
			time -= Time.deltaTime;
		} else {
			gameMaster.SetLocationTitle (title);
			done = true;
		}
	}
}