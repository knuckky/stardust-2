﻿using UnityEngine;
using System.Collections;

public class JumpDustBehaviour : MonoBehaviour {

	private float timer = 2;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (timer > 0) {
			timer -= Time.deltaTime;
		} else {
			Destroy (gameObject);
		}
	}
}
