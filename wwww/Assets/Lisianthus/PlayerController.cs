﻿using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public float speed = 10f;            // The speed that the player will move at.
    public float jumpPower = 6f;
    public float aerialSpeed = 5f;
        
	public GameObject jumpDust;
	public GameObject landDust;
	public GameObject stepDust;
    
    enum state{onGround, Jumping, Falling}; // States
    state playerState; //State tracker
        
    Vector3 movement;                   // The vector to store the direction of the player's movement.
    Animator anim;                      // Reference to the animator component.
    Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    public FootCollider footCol;
    
    public master gameMaster;

	//Animation Bools
        

    void Start ()
    {
         playerRigidbody = GetComponent <Rigidbody> ();

        //itemsCollected = new Item[100];
        
        
        GameObject gameMasterObject = GameObject.FindWithTag("GameController");
        gameMaster = gameMasterObject.GetComponent<master>();
        transform.position = gameMaster.nextPosition;
        
        
        playerState = state.onGround;
        // Set up references.
        anim = GetComponent <Animator> ();
        playerRigidbody = GetComponent <Rigidbody> ();
    }


    void FixedUpdate ()
    {
        if(Input.GetKeyDown("return"))
            Application.LoadLevel(Application.loadedLevel);
            
        if(Input.GetKeyDown("t"))
        {
            speed = 20f;
            jumpPower = 8f;
            aerialSpeed = 20f;
        }
        // Store the input axes.
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

		//Set isWalking animation bool dependent on input
		/*
		if (Mathf.Abs (h) < 0.5 && Mathf.Abs (v) < 0.5) {
			anim.SetBool ("isWalking", false);
		} else {
			anim.SetBool ("isWalking", true);
		}
		*/

		{
			// Create a boolean that is true if either of the input axes is non-zero.
			bool walking = h != 0f || v != 0f;
			
			// Tell the animator whether or not the player is walking.
			anim.SetBool ("IsWalking", walking);
		}
            
        //do some groovy rotation
        if(h != 0 || v != 0)
        {
            Vector3 A = new Vector3(0,0,0);
            A.x = h;
            A.y = v;
            
            Vector3 B = new Vector3(0, 1, 0);
                
            float angleRot = Vector3.Angle(A, B)*h;
            if(h == 0 && v == -1f)
                angleRot = -180f;
            angleRot -= 90;
            Vector3 magic = new Vector3(transform.rotation.x, angleRot, transform.rotation.y);
            transform.rotation = new Quaternion(0,180f,0,0);
            transform.Rotate(magic);
        }
            
            
        // Jumping stuff
        float j = Input.GetAxisRaw("Jump");
            
        if(playerState == state.onGround && j >= 0.5f)
        {
            Vector3 jump = new Vector3(0, jumpPower, 0);
            playerRigidbody.velocity = jump;
            playerState = state.Jumping;
			Instantiate(jumpDust, transform.position, transform.rotation);
        }
        if (playerState == state.Jumping && footCol.collision) 
		{
			playerState = state.onGround;
			//Create cloud of dust when landing
			Instantiate(landDust, transform.position, transform.rotation);
		}
            
        // Move the player around the scene.
        Move (h, v);

        // Turn the player to face the mouse cursor.
        //Turning ();

        // Animate the player.
        //Animating (h, v);
    }


    void Move (float h, float v)
    {
        if(playerState == state.onGround)
            movement.Set (h * 20f * speed * Time.deltaTime, playerRigidbody.velocity.y, v * 20f * speed * Time.deltaTime);
        else
            movement.Set (h * 20f * aerialSpeed * Time.deltaTime, playerRigidbody.velocity.y, v * 20f * aerialSpeed * Time.deltaTime);
            
        // Normalise the movement vector and make it proportional to the speed per second.
        //movement = movement.normalized * speed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        playerRigidbody.velocity = movement;
     }

	//Function called each time Leon takes a step
	void FootStep(){
		if (playerState == state.onGround) {
			Instantiate (stepDust, transform.position, transform.rotation);
		}
	}

	/*
        void Update (float h, float v)
        {
            // Create a boolean that is true if either of the input axes is non-zero.
            bool walking = h != 0f || v != 0f;

            // Tell the animator whether or not the player is walking.
            anim.SetBool ("IsWalking", walking);
        }
	*/
 }