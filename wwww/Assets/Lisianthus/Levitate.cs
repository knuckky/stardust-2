﻿using UnityEngine;
using System.Collections;

public class Levitate : MonoBehaviour {

	public float levMultiplyer = 0.1f;
	Vector3 initialPos;
	Vector3 lev;

	// Use this for initialization
	void Start () {
		initialPos = transform.position;
		lev = new Vector3 (0, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		lev.y = Mathf.Sin (Time.time)*levMultiplyer;
		transform.position = initialPos + lev;
	}
}
