﻿using UnityEngine;
using System.Collections;

public class IncompletePuzzle : MonoBehaviour {

	private GameObject gameMasterObject;
	private master gameMaster;

	// Use this for initialization
	void Awake () {
		gameMasterObject = GameObject.FindWithTag("GameController");
		gameMaster = gameMasterObject.GetComponent<master>();
		
		if (gameMaster.itemsCollected [1] && gameMaster.itemsCollected [4] && gameMaster.itemsCollected [6] && gameMaster.itemsCollected [7] && gameMaster.itemsCollected [10]){
			Destroy (gameObject);
		}
	}
}
