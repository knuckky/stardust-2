﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform playerPos;
	public float cameraTrackingSpeed = 5f;
    
    public float yTarget = 11.3f;
    public float xTarget = -3;
    public float xRotation = 75;
	// Update is called once per frame
	void Update () {
		
		Vector3 target = playerPos.position;
        
		target.y += yTarget;
        target.z += xTarget;
        
        Vector3 rotation = new Vector3(xRotation, 0, 0);
        transform.rotation = new Quaternion(0,0,0,0);
        transform.Rotate(rotation);
		
		target.x = Mathf.Lerp(transform.position.x, target.x, cameraTrackingSpeed*Time.deltaTime);
		target.y = Mathf.Lerp(transform.position.y, target.y, (cameraTrackingSpeed*0.75f)*Time.deltaTime);
		target.z = Mathf.Lerp(transform.position.z, target.z, (cameraTrackingSpeed*0.75f)*Time.deltaTime);
		transform.position = target;
	}
}
