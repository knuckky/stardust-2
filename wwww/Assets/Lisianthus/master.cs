﻿using UnityEngine;
using System.Collections;

public class master : MonoBehaviour 
{

    public enum Items{foyerKeyFrag, foyerPuzzleFrag, Lantern, graveyardKeyFrag, graveyardPuzzleFrag, boilerCenterKeyFrag, boilerCenterPuzzleFrag, boilerChasmRightPuzzleFrag, foyerKeyFrag_2, graveyardKeyFrag_2, boilerDrainLeftPuzzleFrag};

    public bool[] itemsCollected;    
	public Vector3 nextPosition;
    public bool lantern = false;
    public bool loaded = false;
    public float timer;
	public float textTimer; //Time between text fading in and out
	public float fadeSpeed; //Used in screenfading. The rate at which the screen fades in and out
	private int nextScene; //Used for scene change after fade. Assigned via collision with collector and door
    
    GameObject[] masters;
    master otherMaster;

	GUITexture fade; //reference to the guitexture component used for screen fading
	public int isFading = 1;

	GUIText screenText; //Referene for title text component
	public int isTextFading = 0;
	Color screenTextColor = Color.white;
    
    void Awake() {
        DontDestroyOnLoad(transform.gameObject);
        timer = 0;
        itemsCollected = new bool[11];

		fade = GetComponent<GUITexture> (); //Get the guitexture component for reference
		fade.pixelInset = new Rect (0f, 0f, Screen.width, Screen.height);

		screenText = GetComponent<GUIText> (); //Get the guitexture component for reference
		screenText.pixelOffset = new Vector2 (Screen.width / 2, Screen.height / 2); //Draw text in middle of screen
		screenTextColor.a = 0;
		screenText.color = screenTextColor; //Set text to transperant at first


        masters = GameObject.FindGameObjectsWithTag("GameController");
        for(int i = 0; i<masters.Length; i++)
        {
            otherMaster = masters[i].GetComponent<master>();
            if(otherMaster.timer > 3)
            {
                Destroy(gameObject);
            }
        }
    }

	void Update (){ //Used for screen fading
		//SCREEN FADE
		if (isFading == 1){

			//Fade to clear 
			fade.color = Color.Lerp (fade.color, Color.clear, fadeSpeed*Time.deltaTime);
			if (fade.color.a <= 0.05f)
			{
				fade.color = Color.clear;
				isFading = 0;
			}
		}
		else if (isFading == -1){
			//Fade to black and change scene
			fade.color = Color.Lerp (fade.color, Color.black, fadeSpeed*Time.deltaTime);
			if (fade.color.a >= 0.5f)
			{
				fade.color = Color.black;
				isFading = 1;
				Application.LoadLevel (nextScene);
			}
		}

		//TEXT FADE
		if (isTextFading == 1){
			//Fade to clear 
			screenTextColor = screenText.color;
			screenTextColor.a -= fadeSpeed/2*Time.deltaTime;
			screenText.color = screenTextColor;
			if (screenText.color.a <= 0f)
			{
				screenTextColor.a = 0f;
				screenText.color = screenTextColor;
				isTextFading = 0;
			}
		}
		else if (isTextFading == -1){
			float tt = textTimer;
			//Fade to black and change scene
			screenTextColor = screenText.color;
			screenTextColor.a += fadeSpeed*Time.deltaTime;
			screenText.color = screenTextColor;
			if (screenText.color.a >= 1f)
			{
				screenText.color = Color.white;
				if (textTimer > 0){
					textTimer -= Time.deltaTime;
				}
				else
				{
					isTextFading = 1;
					textTimer = tt;
				}
			}
		}
	}
    
    void FixedUpdate ()
    {
    
        if(Input.GetKeyDown("x"))
        {
            for(int i = 0; i < 11; i++)
                {
                    Debug.Log(itemsCollected[i] + " " + itemsCollected.Length);
                }
            Debug.Log("done");
        }
        
        
        if(!loaded && timer < 3)
        {
            timer += Time.deltaTime;
        }
        else if(!loaded && timer >= 3)
        {
            for(int i = 0; i < 11; i++)
            {
                itemsCollected[i] = false;
                Debug.Log(itemsCollected[i] + " " + itemsCollected.Length);
            }
            loaded = true;
        }
    }

	public void SetLocationTitle(string title){
		if (title != screenText.text) {
			screenText.text = title;
			isTextFading = -1;
		}

	}

	public void SetNewScene(int scene){
		nextScene = scene;
	}
	
	//Draw HUD
	void OnGUI(){
		
	}
}