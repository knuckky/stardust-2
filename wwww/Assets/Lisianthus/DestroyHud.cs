﻿using UnityEngine;
using System.Collections;

public class DestroyHud : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GameObject hud = GameObject.FindGameObjectWithTag ("HUD");
		Destroy (hud.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
