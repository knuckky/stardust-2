﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDBehaviour : MonoBehaviour {

	public float fadeSpeed = 0.4f;
	public float timer = 1;
	private float t;
	private int itemFade = 0;
	private Color col = Color.white;
	private Color colGlow = Color.white;

	private GameObject[] huds;
	public float hudTimer;
	private HUDBehaviour otherHud;
	private bool loaded;

	private Image item;
	public GameObject puzzleUI;

	//Sprite graphics
	public Sprite sprPuzzle;
	public Sprite sprLantern;
	public Sprite sprPetal;
	public Sprite sprFlower;

	//Petal Objects
	private GameObject[] petalUI;
	private GameObject[] petals;
	private Image[] petal;

	//Get Game Master
	private GameObject gameMasterObject;
	private master gameMaster;

	//Get Glow Object
	public GameObject glowObject;
	private Image glow;

	void Awake(){
		DontDestroyOnLoad (transform.gameObject);

		//Stop multiple instances of HUD
		huds = GameObject.FindGameObjectsWithTag("HUD");
		for(int i = 0; i<huds.Length; i++)
		{
			otherHud = huds[i].GetComponent<HUDBehaviour>();
			if(otherHud.hudTimer > 3)
			{
				Destroy(gameObject);
			}
		}
	}

	// Use this for initialization
	void Start () {
		//Get itemFade
		item = puzzleUI.GetComponent<Image> ();
		t = timer; //Set timer to default ammount

		col.a = 0; //Make the hud images start transperant
		colGlow.a = 0; //Make glow start transperant

		//Get petals
		petalUI = new GameObject[5];
		petal = new Image[5];
		petals = GameObject.FindGameObjectsWithTag ("UIPetal");
		for (int i = 0; i < petals.Length; i++){
			petalUI[i] = petals[i];
			petal[i] = petalUI[i].GetComponent<Image>();
		}

		//Get Master
		gameMasterObject = GameObject.FindWithTag("GameController");
		gameMaster = gameMasterObject.GetComponent<master>();

		//Get Glow
		glow = glowObject.GetComponent<Image> ();
	}
	
	// Update is called once per frame
	void Update () {
		//Fade in
		if (itemFade == 1) {
			if (col.a < 1){
				col.a += fadeSpeed * Time.deltaTime*2;
			} else {
				col.a = 1;
				if (t > 0){
					t -= Time.deltaTime;
				} else {
					itemFade = -1;
					t = timer;
				}
			}
		//Fade Out
		} else if (itemFade == -1) {
			if (col.a > 0){
				col.a -= fadeSpeed * Time.deltaTime;
			} else {
				col.a = 0;
				itemFade = 0;
			}
		}
		item.color = col; //Set final colour of fade

		//Set petal visibility based on items collected
		for (int i = 0; i < petals.Length; i++){
			switch (i){
			case 0:
				if (gameMaster.itemsCollected[0])
					petal[i].color = Color.white;
				break;
			case 1:
				if (gameMaster.itemsCollected[3])
					petal[i].color = Color.white;
				break;
			case 2:
				if (gameMaster.itemsCollected[5])
					petal[i].color = Color.white;
				break;
			case 3:
				if (gameMaster.itemsCollected[8])
					petal[i].color = Color.white;
				break;
			case 4:
				if (gameMaster.itemsCollected[9])
					petal[i].color = Color.white;
				break;
			}
			//If all key fragments have been collected, turn the petals to red
			if (gameMaster.itemsCollected [0] && gameMaster.itemsCollected [3] && gameMaster.itemsCollected [5] && gameMaster.itemsCollected [8] && gameMaster.itemsCollected [9]){
				petal[i].color = Color.red;
			}
		}

		//Glow Pulse
		if (colGlow.a > 0f) {
			colGlow.a -= 2f * Time.deltaTime;
		} else {
			colGlow.a = 0f;
		}
		glow.color = colGlow;

		//Destroy hud duplicates
		if(!loaded && hudTimer < 3)
		{
			hudTimer += Time.deltaTime;
		}
		else if(!loaded && hudTimer >= 3)
		{
			loaded = true;
		}
	}

	public void FadeIn(){
		itemFade = 1;
	}

	public void FadeOut(){
		itemFade = -1;
	}

	public void SetSprite(int id){
		switch (id){
		case 0:
			item.sprite = sprPuzzle;
			break;
		case 1:
			item.sprite = sprLantern;
			break;
		case 2:
			item.sprite = sprPetal;
			break;
		}
	}

	public void GlowPulse(){
		colGlow.a = 1;
	}
}
