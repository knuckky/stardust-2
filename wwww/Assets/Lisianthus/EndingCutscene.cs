﻿using UnityEngine;
using System.Collections;

public class EndingCutscene : MonoBehaviour {

	private GameObject master;
	
	public void EndGame(){
		master = GameObject.FindWithTag("GameController");
		Destroy(master);
		Application.LoadLevel(0);
	}
}
