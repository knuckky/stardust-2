﻿using UnityEngine;
using System.Collections;

public class FootCollider : MonoBehaviour {

		public bool collision = false;
		
        void OnTriggerEnter(Collider thing)
		{
			if (thing.gameObject.tag == "Floor" || thing.gameObject.tag == "Platform")
			{
				collision = true;
			}

		}
		
		void OnTriggerExit(Collider thing)
		{
			if (thing.gameObject.tag == "Floor" || thing.gameObject.tag == "Platform")
			{
				collision = false;
			}
		}
}