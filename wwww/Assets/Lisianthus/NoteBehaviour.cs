﻿using UnityEngine;
using System.Collections;

public class NoteBehaviour : MonoBehaviour {
	
	public GameObject riddleText;
	private NoteRiddleText riddleScript;


	// Use this for initialization
	void Start () {
		riddleScript = riddleText.GetComponent<NoteRiddleText> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	//Trigger riddle with player proximity
	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Collector") {
			riddleScript.FadeIn();
		}
	}

	//Trigger riddle with player proximity
	void OnTriggerExit(Collider col){
		if (col.gameObject.tag == "Collector") {
			riddleScript.FadeOut();
		}
	}
}
